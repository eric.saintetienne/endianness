/**
 * vim: ai:noet:ts=8:sw=0
 * @file kendian.c
 * @author Eric Saint Etienne
 * @date 8 Apr 2020
 * @brief Linux kernel module endianness test/demo
 */

#include <linux/module.h>

static int __init kendian_start(void)
{
	union {
		u8  u8[8];
		u16 u16[4];
		u32 u32[2];
		u64 u64[1];
	} u;
	unsigned char *p = (unsigned char*) &u;
	int i;

	pr_info("loading module kendian\n");

	/* Initialize memory */
	for (i = 0; i < 8; i++)
		p[i] = (((i*2) & 0xf) << 4) | ((i*2+1) & 0xf);

	pr_info("initialized raw memory:   %02x   %02x   %02x   %02x   %02x   %02x   %02x   %02x  (8 bytes)\n",
		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

	pr_info("      as u8  (%lu byte) : 0x%02x|0x%02x|0x%02x|0x%02x|0x%02x|0x%02x|0x%02x|0x%02x  (8x u8)\n",
		sizeof(u.u8[0]), u.u8[0], u.u8[1], u.u8[2], u.u8[3], u.u8[4], u.u8[5], u.u8[6], u.u8[7]);
	pr_info("      as u16 (%lu bytes):  0x%04x  | 0x%04x  | 0x%04x  | 0x%04x    (4x u16)\n",
		sizeof(u.u16[0]), u.u16[0], u.u16[1], u.u16[2], u.u16[3]);
	pr_info("      as u32 (%lu bytes):     0x%08x     |    0x%08x       (2x u32)\n",
		sizeof(u.u32[0]), u.u32[0], u.u32[1]);
	pr_info("      as u64 (%lu bytes):           0x%016llx             (1x u64)\n",
		sizeof(u.u64[0]), u.u64[0]);

	return 0;
}

static void __exit kendian_end(void)
{
	pr_info("unloading module kendian\n");
}

module_init(kendian_start);
module_exit(kendian_end);
