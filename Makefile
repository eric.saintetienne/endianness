# if KERNELRELEASE is defined, we've been invoked from the
# kernel build system and can use its language.

ifneq ($(KERNELRELEASE),)
obj-m := kendian.o
else

# Otherwise we were called directly from the command line.
# Invoke the kernel build system.

# Edit KERNEL_SOURCE according to your kernel source tree location
KERNEL_SOURCE := /lib/modules/$(shell uname -r)/build/

PWD := $(shell pwd)

kendian:
	$(MAKE) -C $(KERNEL_SOURCE) SUBDIRS=$(PWD) modules

# Userland program
uendian: uendian.o
	gcc -Wall -o uendian uendian.o

uclean:
	$(RM) uendian uendian.o

clean: uclean
	$(MAKE) -C $(KERNEL_SOURCE) SUBDIRS=$(PWD) clean

endif
