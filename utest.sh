#!/bin/bash -e

# 1 if LE, 0 if BE
declare -i LE=$(echo -n I | od -to2 | head -n1 | cut -f2 -d" " | cut -c6)

if ((LE)); then
    ./uendian | grep 0xefcdab8967452301 > /dev/null
	echo "Little-endian test passed"
else
    ./uendian | grep 0x0123456789abcdef > /dev/null
	echo "Big-endian test passed"
fi
