This simple C project demonstrates endianness in userland as well as in (linux) kernel.

[![pipeline status](https://gitlab.com/eric.saintetienne/endianness/badges/master/pipeline.svg)](https://gitlab.com/eric.saintetienne/endianness/-/commits/master)

## Output

### Userland program

#### Little endian machine

```
$ ./uendian 
initialized raw memory:   01   23   45   67   89   ab   cd   ef  (8 bytes)
      as u8  (1 byte) : 0x01|0x23|0x45|0x67|0x89|0xab|0xcd|0xef  (8x u8)
      as u16 (2 bytes):  0x2301  | 0x6745  | 0xab89  | 0xefcd    (4x u16)
      as u32 (4 bytes):     0x67452301     |    0xefcdab89       (2x u32)
      as u64 (8 bytes):           0xefcdab8967452301             (1x u64)
```

#### Big endian machine
```
$ ./uendian 
initialized raw memory:   01   23   45   67   89   ab   cd   ef  (8 bytes)
      as u8  (1 byte) : 0x01|0x23|0x45|0x67|0x89|0xab|0xcd|0xef  (8x u8)
      as u16 (2 bytes):  0x0123  | 0x4567  | 0x89ab  | 0xcdef    (4x u16)
      as u32 (4 bytes):     0x01234567     |    0x89abcdef       (2x u32)
      as u64 (8 bytes):           0x0123456789abcdef             (1x u64)
```

### Linux Kernel module

#### Little endian machine

```
$ sudo insmod kendian.ko; sudo rmmod kendian.ko
[2411373.579622] loading module kendian
[2411373.583202] initialized raw memory:   01   23   45   67   89   ab   cd   ef  (8 bytes)
[2411373.591286]       as u8  (1 byte) : 0x01|0x23|0x45|0x67|0x89|0xab|0xcd|0xef  (8x u8)
[2411373.599193]       as u16 (2 bytes):  0x2301  | 0x6745  | 0xab89  | 0xefcd    (4x u16)
[2411373.607187]       as u32 (4 bytes):     0x67452301     |    0xefcdab89       (2x u32)
[2411373.615180]       as u64 (8 bytes):           0xefcdab8967452301             (1x u64)
[2411374.125654] unloading module kendian
```

#### Big endian machine
```
$ sudo insmod kendian.ko; sudo rmmod kendian.ko
[304252.352852] loading module kendian
[304252.352865] initialized raw memory:   01   23   45   67   89   ab   cd   ef  (8 bytes)
[304252.352874]       as u8  (1 byte) : 0x01|0x23|0x45|0x67|0x89|0xab|0xcd|0xef  (8x u8)
[304252.352881]       as u16 (2 bytes):  0x0123  | 0x4567  | 0x89ab  | 0xcdef    (4x u16)
[304252.352887]       as u32 (4 bytes):     0x01234567     |    0x89abcdef       (2x u32)
[304252.352893]       as u64 (8 bytes):           0x0123456789abcdef             (1x u64)
[304252.862865] unloading module kendian
```

## Build

### Userland program

#### Environment
Usual `gcc`/`make` build environment needs to be setup. In a nutshell:

##### Redhat derivatives:
```
sudo dnf groupinstall -y 'Development Tools'
```

##### debian compatible:
```
sudo apt-get install -y build-essential
```

#### Build the program
```
$ make uendian
gcc -Wall -o uendian uendian.o
```

The program is now available as `uendian`.

### Linux Kernel module

#### Environment

In addition to the standard build environment used for the userland program above, some amount of kernel sources are necessary:

##### Redhat derivatives:
Install the current kernel sources under `/usr/src/kernels/`:
```
sudo dnf install -y kernel-devel-$(uname-r)
```

##### debian compatible:
Install the current kernel headers:
```
sudo apt-get install kernel-headers-$(uname -r)
```

#### Build the linux kernel module

If building against an existing kernel source tree not accessible via `/lib/modules/` then you need to edit `KERNEL_SOURCE` in `Makefile`.

```
# make kendian
make -C /lib/modules/4.14.35+/build/ SUBDIRS=/root/endian modules
make[1]: Entering directory `/root/u3'
  CC [M]  /root/endian/kendian.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /root/endian/kendian.mod.o
  LD [M]  /root/endian/kendian.ko
make[1]: Leaving directory `/root/u3'
```

The linux kernel module is now available as `kendian.ko`

### Cleanup
```
# make clean
rm -f uendian uendian.o
make -C /lib/modules/4.14.35+/build/ SUBDIRS=/root/endian clean
make[1]: Entering directory `/root/u3'
  CLEAN   /root/endian/.tmp_versions
  CLEAN   /root/endian/Module.symvers
make[1]: Leaving directory `/root/u3'
```

Alternatively `make uclean` will clean the userland part only.

&copy; 2020, Eric Saint Etienne
